using System;
using Xunit;

namespace ADPP.Po.Algoritmes.BinarySearchTree.Tests
{
    public class UnitTest1
    {
        private BinaryTree SimpleTree()
        {
            BinaryTree tree = new BinaryTree(10);
            tree.Insert(13);
            tree.Insert(6);
            tree.Insert(3);
            tree.Insert(2);
            tree.Insert(12);

            return tree;
        }
        [Fact]
        public void Inserting_A_Lower_Value_Sets_The_Added_Value_As_Left_Element_In_The_Tree_Should_Be_True()
        {
            BinaryTree tree = new BinaryTree(10);
            tree.Insert(6);

            Assert.True(tree.Find(10).LeftChild.Value == 6);
        }

        [Fact]
        public void Inserting_A_Higher_Value_Sets_The_Added_Value_As_Left_Element_In_The_Tree_Should_Be_True()
        {
            BinaryTree tree = new BinaryTree(10);
            tree.Insert(13);

            Assert.True(tree.Find(10).RightChild.Value == 13);
        }

        [Fact]
        public void Removing_The_Root_Element_Should_Throw_InvalidOperationException_When_Has_Childitems()
        {
            BinaryTree tree = SimpleTree();
            Assert.Throws<InvalidOperationException>(() => tree.Remove(10));
        }

        [Fact]
        public void Removing_A_Node_With_A_Left_Child_Item_Should_Set_The_Parent_Of_The_Left_Child_To_The_New_Parent()
        {
            BinaryTree tree = SimpleTree();

            //When remove 3, the parent of 2 is 6.
            tree.Remove(3);

            Assert.True(tree.Find(2).Parent.Value == 6);
        }

        [Fact]
        public void Removing_A_Node_With_A_Left_And_Right_Child_Item_Should_Set_The_Parent_Of_The_Right_Child_To_The_Left_Child()
        {
            BinaryTree tree = SimpleTree();

            //When remove 6, the parent of 3 should be 6 and the parent of 2 should be 3
            tree.Remove(6);

            Assert.True(tree.Find(3).Parent.Value == 10 && tree.Find(2).Parent.Value == 3);
        }
    }
}
