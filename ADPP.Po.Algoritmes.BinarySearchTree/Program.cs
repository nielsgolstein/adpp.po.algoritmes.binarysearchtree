﻿using System;

namespace ADPP.Po.Algoritmes.BinarySearchTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree tree = new BinaryTree(10);

            tree.Insert(13);
            tree.Insert(6);
            tree.Insert(3);
            tree.Insert(2);
            tree.Insert(12);

            Console.WriteLine(tree.Find(13).Value);
            Console.ReadLine();
        }
    }
}
