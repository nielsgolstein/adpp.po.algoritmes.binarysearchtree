﻿using ADPP.Po.Algoritmes.BinarySearchTree.Abstraction;
using System;

namespace ADPP.Po.Algoritmes.BinarySearchTree
{
    /// <summary>
    /// A node of the tree containing a KEY. The value is not added in this implementation since the KEY is also the VALUE here.
    /// </summary>
    public class BinaryTreeNode : IBinaryTreeNode
    {
        public int Depth { get; }
        public int NodeSize { get; }
        public bool IsLeaf { get { return LeftChild == null && RightChild == null;  } }
        public bool IsRoot { get; set; }
        public bool IsOnLeftSide 
        { 
            get
            {
                return this.Value < this.Parent.Value;
            } 
        }

        public bool IsOnRightSide
        {
            get
            {
                return this.Value >= this.Parent.Value;
            }
        }

        public IBinaryTreeNode LeftChild { get; set; }
        public IBinaryTreeNode RightChild { get; set; }
        public IBinaryTreeNode Parent { get; set; }

        public int Value { get; }

        public BinaryTreeNode(int value, IBinaryTreeNode parent)
        {
            this.Value = value;
            this.Parent = parent;
            this.Depth = Parent != null ? Parent.Depth + 1 : 0;
        }

        ///<summary>
        ///Inserts a new value to the database
        ///</summary>
        public IBinaryTreeNode Insert(int toInsertValue)
        {
            // Wanneer de toInsertValue kleiner is dan de waarde van de parent (this), plaats links. Anders rechts.
            if (toInsertValue < this.Value)
            {
                if (LeftChild == null)
                {
                    this.LeftChild = new BinaryTreeNode(toInsertValue, this); //Set child since it's null
                    return this.LeftChild;
                }
                else
                    LeftChild.Insert(toInsertValue); //Repeat this process.
            }
            else
            {
                if (RightChild == null)
                {
                    this.RightChild = new BinaryTreeNode(toInsertValue, this); //Set child since it's null
                    return this.RightChild;
                }
                else
                    RightChild.Insert(toInsertValue); //Repeat this process.
            }

            return null;
        }


        public void Remove()
        {
            //When this node is the root, we pick a new root based on the possible children of the root.
            if(this.IsRoot)
            {
                if (this.IsLeaf)
                    return; //No childen, we can simply unsert the object in a more high code level.
                else
                    throw new InvalidOperationException("The root cannot be deleted while there are still child items.");
            }

            bool removedItemIsOnLeftSide = this.IsOnLeftSide;

            if (this.IsOnLeftSide)
                this.Parent.LeftChild = null; // We unset the parent it's LEFT child since we know  it is equal to THIS specific node.
            else
                this.Parent.RightChild = null; // We unset the parent it's RIGHT child since we know it is equal to is THIS specific node.

            //No children
            if (this.IsLeaf)
            {
                return;
            }

            if (this.RightChild == null && this.LeftChild != null)
                this.LeftChild.SetParent(this.Parent);  //Only contains a Left child, we need to set the parent of the left child to the parent of THIS to remove class.
            else if (this.RightChild != null && this.LeftChild == null)
                this.RightChild.SetParent(this.Parent); //Only contains a Right child, we need to set the parent of the left child to the parent of THIS to remove class.
            else
            {
                this.LeftChild.SetParent(this.RightChild);
                this.RightChild.SetParent(this.Parent);
            }
        }

        ///<summary>
        ///Sets the new parent and also updates the parent it's child.
        ///</summary>
        public void SetParent(IBinaryTreeNode parent)
        {
            if (parent == null)
            {
                this.Parent = null;
                return;
            }

            //If the value is smaller than the parent value, this should be the left child.
            if (this.Value < parent.Value)
                if(parent.LeftChild == null)
                {
                    parent.LeftChild = this;
                }
                else
                {
                    // The parent element already has a left child, we cannot override so we delegate this through 
                    //using a recursing function, placing the node UNDER the parent it's left child.
                    SetParent(parent.LeftChild);
                    return;
                }
            else
                if (parent.RightChild == null)
                {
                    parent.RightChild = this;
                }
                else
                {
                    SetParent(parent.RightChild);
                    return;
                }

            this.Parent = parent;
        }


        #region Find

        public IBinaryTreeNode Find(int toFindValue)
        {
            //check if we found it
            if (toFindValue == this.Value)
                return this;

            if (toFindValue < this.Value)
            {
                if (LeftChild == null)
                    return null; //Not found
                else
                    return LeftChild.Find(toFindValue); //Repeat this process.
            }
            else
            {
                if (RightChild == null)
                    return null; //Not found
                else
                    return RightChild.Find(toFindValue); //Repeat this process.
            }
        }

        public IBinaryTreeNode FindMin(IBinaryTreeNode node)
        {
            // The Min value is always on The most left position.
            return node.LeftChild == null ? node : FindMin(node.LeftChild);
        }

        public IBinaryTreeNode FindMax(IBinaryTreeNode node)
        {
            // The Max value is always on The most right position.
            return node.RightChild == null ? node : FindMax(node.RightChild);
        }

        #endregion
    }
}
