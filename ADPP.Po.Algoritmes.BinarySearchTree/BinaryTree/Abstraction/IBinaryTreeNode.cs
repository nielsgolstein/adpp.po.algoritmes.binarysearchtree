﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.BinarySearchTree.Abstraction
{
    public interface IBinaryTreeNode
    {
        /// <summary>
        /// The Depth of the node into the tree. The depth is the distance from the root to this current node through the edges.
        /// </summary>
        int Depth { get; }

        /// <summary>
        /// Determines if this node is a Leaf, the node is a leaf when it has NO child elements
        /// </summary>
        bool IsLeaf { get; }

        /// <summary>
        /// Determines if this node is a root, the node is a root when it has NO parent.
        /// </summary>
        bool IsRoot { get; set; }

        /// <summary>
        /// The left child of the node
        /// </summary>
        IBinaryTreeNode LeftChild { get; set; }

        /// <summary>
        /// The right child of the node
        /// </summary>
        IBinaryTreeNode RightChild { get; set; }

        /// <summary>
        /// The parent of the node (Nullable).
        /// </summary>
        IBinaryTreeNode Parent { get; set; }

        /// <summary>
        /// The value of the node.
        /// </summary>
        int Value { get; }

        /// <summary>
        /// Inserts a new value into the tree. When the provided value is already in the tree, an exception is thrown.
        /// </summary>
        /// <param name="toInsertValue">The value to insert</param>
        IBinaryTreeNode Insert(int toInsertValue);

        /// <summary>
        /// Removes a KEY from the tree.
        /// </summary>
        /// <param name="toInsertValue">The key to remove from the tree</param>
        void Remove();

        /// <summary>
        /// Finds a specific key in the tree.
        /// </summary>
        /// <param name="toFindValue">The key to find</param>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        IBinaryTreeNode Find(int toFindValue);

        /// <summary>
        /// Finds the minimum key in the tree and returns it
        /// </summary>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        IBinaryTreeNode FindMin(IBinaryTreeNode node);

        /// <summary>
        /// Finds the maximum key in the tree and returns it.
        /// </summary>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        IBinaryTreeNode FindMax(IBinaryTreeNode node);


        void SetParent(IBinaryTreeNode parent);
    }
}
