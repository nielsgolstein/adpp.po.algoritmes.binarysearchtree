﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.BinarySearchTree
{
    /// <summary>
    /// A root tree node contains all the values of a <see cref="BinaryTreeNode"/> but has as extension the IsRoot = true.
    /// </summary>
    public class BinaryTreeRootNode : BinaryTreeNode
    {
        /// <summary>
        /// Creates a <see cref="BinaryTreeRootNode"/>.
        /// </summary>
        /// <param name="value">The key of the node</param>
        public BinaryTreeRootNode(int value) : base(value, null) //Parent can be NULL since it's the root item.
        {
            this.IsRoot = true;
        }
    }
}
