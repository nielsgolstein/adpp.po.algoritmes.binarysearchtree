﻿using ADPP.Po.Algoritmes.BinarySearchTree.Abstraction;
using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.BinarySearchTree
{
    /// <summary>
    /// The binary tree object
    /// </summary>
    public class BinaryTree
    {
        /// <summary>
        /// The binary tree contains a root node.
        /// </summary>
        private BinaryTreeRootNode root;

        /// <summary>
        /// Creates a new <see cref="BinaryTree"/> with a single value. This value is used as root value as begin of the tree.
        /// </summary>
        /// <param name="value">The specified value which will be used as root element.</param>
        public BinaryTree(int value)
        {
            // The root of the tree is the provided value.
            this.root = new BinaryTreeRootNode(value);
        }

        /// <summary>
        /// Inserts a new value into the tree. When the provided value is already in the tree, an exception is thrown.
        /// </summary>
        /// <param name="toInsertValue">The value to insert</param>
        public void Insert(int toInsertValue)
        {
            IBinaryTreeNode node = root.Find(toInsertValue);
            if (node != null)
                throw new InvalidOperationException("The key is already present.");

            this.root.Insert(toInsertValue);
        }

        /// <summary>
        /// Removes a KEY from the tree.
        /// </summary>
        /// <param name="toInsertValue">The key to remove from the tree</param>
        public void Remove(int toInsertValue)
        {
            IBinaryTreeNode node = root.Find(toInsertValue);
            if (node == null)
                return;

            node.Remove();
            node = null;
        }

        /// <summary>
        /// Finds a specific key in the tree.
        /// </summary>
        /// <param name="toFindValue">The key to find</param>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        public IBinaryTreeNode Find(int toFindValue)
        {
            return this.root.Find(toFindValue);
        }

        /// <summary>
        /// Finds the minimum key in the tree and returns it
        /// </summary>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        public IBinaryTreeNode FindMin()
        {
            return this.root.FindMin(root);
        }

        /// <summary>
        /// Finds the maximum key in the tree and returns it.
        /// </summary>
        /// <returns><see cref="IBinaryTreeNode"/></returns>
        public IBinaryTreeNode FindMax()
        {
            return this.root.FindMax(root);
        }
    }
}
